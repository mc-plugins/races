package me.bumblebeee_.races;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public class Runnables {
	
	public static HashMap<UUID, String> races = new HashMap<UUID, String>();
	
	@SuppressWarnings("deprecation")
	public static void raceEffects() {
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Races.instance, new BukkitRunnable() {
			public void run() {
				for (Player p : Bukkit.getServer().getOnlinePlayers()) {
					if (races.containsKey(p.getUniqueId())) {
						String race = races.get(p.getUniqueId());
						if (race.equalsIgnoreCase("elf")) {
							p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 140, 0));
						} else if (race.equalsIgnoreCase("dwarf")) {
							p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 140, 0));
						} else if (race.equalsIgnoreCase("human")) {
							p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 140, 0));
						} else if (race.equalsIgnoreCase("ogre")) {
							p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 140, 0));
						} else {
							YamlConfiguration c = Config.getPlayers();
							c.set("players." + p.getUniqueId(), null);
							Config.savePlayers(c);
						}
					}
				}
			}
		}, 10, 10);
	}

}
