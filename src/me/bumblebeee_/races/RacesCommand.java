package me.bumblebeee_.races;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class RacesCommand implements CommandExecutor {

	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		
		if (cmd.getName().equalsIgnoreCase("setrace")) {
			if (!sender.hasPermission("races.setrace")) {
				sender.sendMessage(ChatColor.RED + "You do not have the required permissions!");
				return true;
			}
			
			if (args.length != 2) {
				sender.sendMessage(ChatColor.RED + "Invalid arguments. Correct usage: /setrace <player> <race>");
				return true;
			}
			
			ArrayList<String> races = new ArrayList<String>(Arrays.asList("elf", "dwarf", "human", "ogre"));
			
			String race = args[1];
			
			if (!races.contains(race.toLowerCase())) {
				sender.sendMessage(ChatColor.RED + "Invalid race name!");
				return true;
			}
			
			File f = new File(Races.instance.getDataFolder(), "players.yml");
			YamlConfiguration c = YamlConfiguration.loadConfiguration(f);
			
			Player t = Bukkit.getServer().getPlayer(args[0]);
			if (t != null) {
				c.set("players." + t.getUniqueId(), race);
				sender.sendMessage(ChatColor.GREEN + "Successfully changed " + t.getName() + "'s race to " + race + "!");
				t.sendMessage(ChatColor.GREEN + "Your race has been changed to " + race + "!");
				Runnables.races.remove(t.getUniqueId());
				Runnables.races.put(t.getUniqueId(), race);
			} else {
				OfflinePlayer ot = Bukkit.getServer().getOfflinePlayer(args[0]);
				c.set("players." + ot.getUniqueId(), race);
				sender.sendMessage(ChatColor.GREEN + "Successfully changed " + ot.getName() + "'s race to " + race + "!");
				Runnables.races.remove(ot.getUniqueId());
				Runnables.races.put(ot.getUniqueId(), race);
			}
			try {
				c.save(f);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return true;
	}
	
}
