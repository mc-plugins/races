package me.bumblebeee_.races;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class Inv {

	public static void open(Player p) {
		Inventory inv = Bukkit.getServer().createInventory(null, 9, "Select a race!");
		
		ItemStack filler = new ItemStack(Material.STAINED_GLASS_PANE);
		ItemMeta fm = filler.getItemMeta();
		fm.setDisplayName(" ");
		filler.setItemMeta(fm);
		
		ItemStack elf = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 4);
		ItemMeta em = elf.getItemMeta();
		ArrayList<String> emlore = new ArrayList<String>(Arrays.asList(ChatColor.GRAY + "Speed I"));
		em.setLore(emlore);
		em.setDisplayName(ChatColor.DARK_PURPLE + "Choose the Elf race.");
		elf.setItemMeta(em);

		ItemStack dwarf = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 12);
		ItemMeta dm = dwarf.getItemMeta();
		ArrayList<String> dmlore = new ArrayList<String>(Arrays.asList(ChatColor.GRAY + "Haste I"));
		dm.setLore(dmlore);
		dm.setDisplayName(ChatColor.DARK_PURPLE + "Choose the Dwarf race.");
		dwarf.setItemMeta(dm);

		ItemStack human = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 11);
		ItemMeta hm = human.getItemMeta();
		ArrayList<String> hmlore = new ArrayList<String>(Arrays.asList(ChatColor.GRAY + "Regeneration I"));
		hm.setLore(hmlore);
		hm.setDisplayName(ChatColor.DARK_PURPLE + "Choose the Human race.");
		human.setItemMeta(hm);

		ItemStack ogre = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 14);
		ItemMeta om = ogre.getItemMeta();
		ArrayList<String> omlore = new ArrayList<String>(Arrays.asList(ChatColor.GRAY + "Strength I"));
		om.setLore(omlore);
		om.setDisplayName(ChatColor.DARK_PURPLE + "Choose the Ogre race.");
		ogre.setItemMeta(om);
		
		inv.setItem(1, elf);
		inv.setItem(3, dwarf);
		inv.setItem(5, human);
		inv.setItem(7, ogre);
		inv.setItem(0, filler);
		inv.setItem(2, filler);
		inv.setItem(4, filler);
		inv.setItem(6, filler);
		inv.setItem(8, filler);
		p.openInventory(inv);
	}

}
