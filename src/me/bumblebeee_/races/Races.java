package me.bumblebeee_.races;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import me.bumblebeee_.races.events.RegisterEvents;
import net.md_5.bungee.api.ChatColor;

public class Races extends JavaPlugin {
	
	public static Plugin instance = null;
	
	public void onEnable() {
		instance = this;
		getCommand("setrace").setExecutor(new RacesCommand());
		Config.createPlayers();
		saveDefaultConfig();
		RegisterEvents.register();
		Runnables.raceEffects();
		Recipes.register();
		
		for (Player p : Bukkit.getServer().getOnlinePlayers()) {
			File f = new File(getDataFolder(), "players.yml");
			YamlConfiguration c = YamlConfiguration.loadConfiguration(f);
			String race = c.getString("players." + p.getUniqueId().toString());
			
			if (race != null) {
				Runnables.races.put(p.getUniqueId(), race);
			} else {
				if (getConfig().getBoolean("no-race-kick") == true) {
					p.kickPlayer(ChatColor.RED + "A error has occured! You have not selected a race!");
			
				}
			}
		}
	}
}
