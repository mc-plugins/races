package me.bumblebeee_.races;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class Recipes {
	
	public static void register() {
		item();
	}
	
	public static void item() {
		ItemStack result = new ItemStack(Material.EYE_OF_ENDER);
		ItemMeta rm = result.getItemMeta();
		rm.setDisplayName(ChatColor.DARK_PURPLE + "Right click to change your race!");
		result.setItemMeta(rm);
		
		ShapedRecipe recipe = new ShapedRecipe(result);
		recipe.shape("PGP", "BPB", "PGP");
		recipe.setIngredient('P', Material.ENDER_PEARL);
		recipe.setIngredient('G', Material.GOLD_INGOT);
		recipe.setIngredient('B', Material.BLAZE_POWDER);
		Races.instance.getServer().addRecipe(recipe);
	}

}
