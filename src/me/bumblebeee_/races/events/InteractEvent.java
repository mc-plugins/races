package me.bumblebeee_.races.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import me.bumblebeee_.races.Inv;

public class InteractEvent implements Listener {
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_AIR) {
			if (e.getItem() != null) {
				if (e.getItem().hasItemMeta()) {
					if (e.getItem().getItemMeta().getDisplayName().equals("§5Right click to change your race!")) {
						Inv.open(e.getPlayer());
						e.getPlayer().getInventory().remove(e.getItem());
					}
				}
			}
		}
	}

}
