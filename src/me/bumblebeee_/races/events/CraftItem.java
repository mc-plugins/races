package me.bumblebeee_.races.events;

import java.util.Random;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;

import me.bumblebeee_.races.Races;

public class CraftItem implements Listener {
	
	Random r = new Random();
	
	@EventHandler
	public void onItemCraft(CraftItemEvent e) {
		if (e.getRecipe().getResult().getItemMeta().getDisplayName().equals("§5Right click to change your race!")) {
			int rChance = 100;
			int success = r.nextInt(rChance);
			int chance = Races.instance.getConfig().getInt("crafting-success-percent");
			
			if (success > chance) {
				e.setCancelled(true);
				e.getInventory().clear();
			}
		}
	}

}
