package me.bumblebeee_.races.events;

import java.io.File;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

import me.bumblebeee_.races.Races;
import net.md_5.bungee.api.ChatColor;

public class InventoryClose implements Listener {
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		if (e.getInventory().getName().equals("Select a race!")) {
			File f = new File(Races.instance.getDataFolder(), "players.yml");
			YamlConfiguration c = YamlConfiguration.loadConfiguration(f);
			if (c.getString("players." + e.getPlayer().getUniqueId().toString()) == null) {
				((Player) e.getPlayer()).kickPlayer(ChatColor.RED + "You must choose a race!");
			}
		}
	}

}
