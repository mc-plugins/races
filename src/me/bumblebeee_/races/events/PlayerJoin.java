package me.bumblebeee_.races.events;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

import me.bumblebeee_.races.Config;
import me.bumblebeee_.races.Inv;
import me.bumblebeee_.races.Races;
import me.bumblebeee_.races.Runnables;

public class PlayerJoin implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerJoin(final PlayerJoinEvent e) {
		File f = new File(Races.instance.getDataFolder(), "players.yml");
		YamlConfiguration c = YamlConfiguration.loadConfiguration(f);
		String uuid = c.getString("players." + e.getPlayer().getUniqueId().toString());
		if (uuid == null) {
			Bukkit.getServer().getScheduler().runTaskLater(Races.instance, new BukkitRunnable() {
				public void run() {
					Inv.open(e.getPlayer());
				}
				
			}, 1);
		} else {
			String race = c.getString("players." + e.getPlayer().getUniqueId().toString());
			Config.savePlayers(c);
			Runnables.races.put(e.getPlayer().getUniqueId(), race);
		}
	}

}
