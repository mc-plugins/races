package me.bumblebeee_.races.events;

import org.bukkit.Bukkit;

import me.bumblebeee_.races.Races;

public class RegisterEvents {
	
	public static void register() {
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerJoin(), Races.instance);
		Bukkit.getServer().getPluginManager().registerEvents(new InventoryClick(), Races.instance);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerLeave(), Races.instance);
		Bukkit.getServer().getPluginManager().registerEvents(new InventoryClose(), Races.instance);
		Bukkit.getServer().getPluginManager().registerEvents(new CraftItem(), Races.instance);
		Bukkit.getServer().getPluginManager().registerEvents(new InteractEvent(), Races.instance);
	}

}
