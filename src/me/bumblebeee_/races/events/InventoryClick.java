package me.bumblebeee_.races.events;

import java.io.File;
import java.io.IOException;

import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import me.bumblebeee_.races.Races;
import me.bumblebeee_.races.Runnables;
import net.md_5.bungee.api.ChatColor;

public class InventoryClick implements Listener {
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		if (e.getInventory().getName().equals("Select a race!")) {
			ItemStack clicked = e.getCurrentItem();
			Player p = (Player) e.getWhoClicked();
			File f = new File(Races.instance.getDataFolder(), "players.yml");
			YamlConfiguration c = YamlConfiguration.loadConfiguration(f);
			
			String race = "";
			
			if (clicked.getType() == Material.STAINED_GLASS_PANE) {
				if (clicked.getData().getData() == 4) {
					race = "Elf";
				} else if (clicked.getData().getData() == 12) {
					race = "Dwarf";
				} else if (clicked.getData().getData() == 11) {
					race = "Human";
				} else if (clicked.getData().getData() == 14) {
					race = "Ogre";
				}
			}
			
			e.setCancelled(true);
			if (Runnables.races.containsKey(p.getUniqueId())) Runnables.races.remove(p.getUniqueId());
			Runnables.races.put(p.getUniqueId(), race);
			c.set("players." + p.getUniqueId().toString(), race);
			try {
				c.save(f);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			p.closeInventory();
			p.sendMessage(ChatColor.GREEN + "You have choosen the " + race + " race!");
		}
	}

}
