package me.bumblebeee_.races.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import me.bumblebeee_.races.Runnables;

public class PlayerLeave implements Listener {
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent e) {
		Runnables.races.remove(e.getPlayer().getUniqueId());
	}

}
