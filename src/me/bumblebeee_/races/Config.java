package me.bumblebeee_.races;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

public class Config {
	
	public static void createPlayers() {
		try {
			if (!Races.instance.getDataFolder().exists()) {
                Races.instance.getDataFolder().mkdirs();
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
		File f = new File(Races.instance.getDataFolder(), "players.yml");
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				Bukkit.getServer().getLogger().severe("[Races] Failed to create players.yml!");
				e.printStackTrace();
			}
		}
	}
	
	public static YamlConfiguration getPlayers() {
		File file = new File(Races.instance.getDataFolder(), "players.yml");
		return YamlConfiguration.loadConfiguration(file);
	}
	
	public static void savePlayers(YamlConfiguration c) {
		File f = new File(Races.instance.getDataFolder(), "players.yml");
		try {
			c.save(f);
		} catch (IOException e) {
			Bukkit.getLogger().severe("[Races] Failed to save players.yml!");
		}
	}
	
	public static List<String> getList(String path) {
		return Races.instance.getConfig().getStringList("players");
	}

}
